# Introdução

## Dados Iniciais

### Localização

Rua xxxx, xxx  
Bairro xxx  
Ruas próximas xxx, xxx , xxx  
CEP xxx  
Pedro Juan Caballero - Paraguay  

### Metragem

Frente: xxx
Frente ao fundo: xxx
Área: xxx
Perfil do terreno: caimento para a xxx

### Tipo de construção

Residencial e comercial, número de pavimentos suficientes para cerca de 20 apartamentos pequenos.

### Designação dos cômodos

1. Residencial

    **Projetar para cerca de 20 apartamentos pequenos** para estudantes, no máximo 2 pessoas. Banheiro com lavanderia, cozinha compacta e  quarto.

    Metragem aproximada 35m2 por apartamento.

    Acabamento: xxx

2. Comercial

   Utilizar esquina e acesso à avenida de grande visibilidade e movimentação para área comercial. Projetar salas comerciais com pé direito duplo para depósito ou escritório. **Exigência da esquina para loja de troca de óleo.**

   Metragem aproximada 60m2 por sala.

   Acabamento: xxx


## Dados do lote

### Utilizades públicas existentes

* Água xxx (poços xxx)
* Esgoto xxx
* Luz (voltagem xxx, fases xxx, favor ou contra xxx)
* Iluminação pública
* Pavimentaçao asfáltica
* Telefone xxx
* Gás xxx
* Número de casas vizinhas ao lote 

### Leis urbanas (Uso e ocupação do solo)

* Área que pode ser edificada xxx
* Coeficientes de aproveitamento
* Recuo mínimo de frente xxx
* Recuo mínimo lateral xxx
* Recuo mínimo de fundo xxx
* Altura máxima edificio xxx
* Existência de projeto de desapropriação, alargamento de rua ou outras melhorias xxx
* Hora de insolação
* Meio ambiente
* Janelas