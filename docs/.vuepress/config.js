module.exports = {
  dest: 'public',
  plugins: [
    ['@vuepress/plugin-back-to-top'], 
    ['@vuepress/active-header-links', 
      {
        sidebarLinkSelector: '.sidebar-link',
        headerAnchorSelector: '.header-anchor',
        headerTopOffset: 120
      }
    ],
    [
      '@vuepress/google-analytics',
      {
        'ga': 'UA-89398082-3' // UA-00000000-0
      }
    ],
    [
      'vuepress-plugin-medium-zoom'
    ],
    ['@vuepress/nprogress'],
    ['vuepress-plugin-smooth-scroll'],
    ['vuepress-plugin-mathjax']
  ],
  title: 'Projeto residencial Alphaville 4',
  description: 'Apresentação e Projeto arquitetônico e estrutural',
  themeConfig: {
    repo: 'https://gitlab.com/yurifbeck/alphaville4',
    docsDir: 'docs',
    // Customising the header label
    // Defaults to "GitHub"/"GitLab"/"Bitbucket" depending on `themeConfig.repo`
    repoLabel: 'Codigo Fonte',

    // if your docs are in a specific branch (defaults to 'master'):
    docsBranch: 'master',
    // defaults to false, set to true to enable
    editLinks: true,
    // custom text for edit link. Defaults to "Edit this page"
    editLinkText: 'Editar página no GitLab!',
    nav: [
      {
        text: 'Apresentação',
        link: '/apresentacao/'
      },
      {
        text: 'Projeto arquitetônico',
        link: '/arquitetonico/',
        items: [
          { text: 'Concepção', link: '/arquitetonico/concepcao/' },
          { text: 'Interior', link: '/arquitetonico/interior/'} 
        ]
      },
      {
        text: 'Projeto Estrutural',
        link: '/estrutural/'
      },
      {
        text: 'Sobre',
        link: '/sobre/'
      }
    ],
    sidebar: 'auto',
    smoothScroll: true,
    lastUpdated: 'Última modificação',
    searchPlaceholder: 'Pesquisar...'
  }
}