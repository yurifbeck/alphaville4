---
home: true
heroText: Projeto residencial 
heroImage: https://res.cloudinary.com/boloko/image/upload/v1574099672/alphaville4/karwf4tvj96sliyvumhh.jpg
tagline: Projeto residencial no condomínio Alphaville 4 Campo Grande 
actionText: Apresentação →
actionLink: "./apresentacao.md"
# features:
# - title: Localização
#   details: Localizado na linha, esquina, em uma das principais avenidas
# - title: Design
#   details: Design moderno e prático, ideal para estudantes
# - title: Rentável
#   details: Edifício une área comércial e residencial
footer: Produzido por FRBK Engenharia

---
